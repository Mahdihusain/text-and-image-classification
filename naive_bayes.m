clear;
clc;

train_data = importdata('r8-train-all-terms-new.txt');
test_data = importdata('r8-test-all-terms-new.txt');
dictionary = containers.Map;

label = zeros(1,8);
phiy = zeros(1,8);
        
for i=1:length(train_data)
    document = strsplit(char(train_data(i)));
    
    li = labelIndex(char(document(1,1)));
    phiy(1,li) = phiy(1,li) + 1;
    label(1,li) = label(1,li) + length(document) -1;
        
    for j=2:length(document)
        
        if dictionary.isKey(char(document(1,j)))
            x = dictionary(char(document(1,j)));
            x(1,li) = x(1,li) +1;
            dictionary(char(document(1,j))) = x;
        else    
            x = [1 1 1 1 1 1 1 1];
            x(1,li) = x(1,li) +1;
            dictionary(char(document(1,j))) = x;
        end
    end    
    
end

for i=1:length(test_data)
    document = strsplit(char(test_data(i)));
        
    for j=2:length(document)
        
        if dictionary.isKey(char(document(1,j)))==false
            dictionary(char(document(1,j))) = [1 1 1 1 1 1 1 1];
        end
    end    
    
end


%{
majority = 0;
majorityY = 0;

for i=1:8
    if phiy(1,i)>majority || i==1
        majority = phiy(1,i);
        majorityY = i;
    end
end
%}

label = label + ones(1,8)*(dictionary.size(1));
phiy = phiy/length(train_data);


%prediction

success = 0;
confusion = zeros(8);
%majority_success = 0;

for i=1:length(test_data)
    document = strsplit(char(test_data(i)));
    
    realLabel = labelIndex(char(document(1,1)));
    
    %{
    if realLabel==majorityY
        majority_success = majority_success +1;
    end
    %}
    
    maxvalue = 0;
    maxY = 0;

    for y=1:8
        cmaxvalue = log(phiy(1,y));
        for j=2:length(document)
                x = dictionary(char(document(1,j)));
                value  = log(x(1,y)/label(1,y));
                cmaxvalue = cmaxvalue + value;
        end
        
        if y==1 || cmaxvalue>maxvalue
            maxY = y;
            maxvalue = cmaxvalue;
        end
    end
    
    confusion(realLabel,maxY) = confusion(realLabel,maxY) +1;
    
    if realLabel==maxY
        success = success +1;
    end
end
    
test_accuracy = (success/length(test_data))*100;
confusion

%majority_accuracy = (majority_success/length(test_data))*100;

%{
%train data accuracy
success = 0;

for i=1:length(train_data)
    document = strsplit(char(train_data(i)));
    
    realLabel = labelIndex(char(document(1,1)));
    maxvalue = 0;
    maxY = 0;

    for y=1:8
        cmaxvalue = log(phiy(1,y));
        for j=2:length(document)
            x = dictionary(char(document(1,j)));
            value  = log(x(1,y)/label(1,y));
            cmaxvalue = cmaxvalue + value;
        end
        
        if y==1 || cmaxvalue>maxvalue
            maxY = y;
            maxvalue = cmaxvalue;
        end
    end
    
    if realLabel==maxY
        success = success +1;
    end
end
    
train_accuracy = (success/length(train_data))*100;
%}

function index = labelIndex(x)
   if strcmp(x,'acq')==1
       index = 1;
   elseif strcmp(x,'crude')==1
       index = 2;
   elseif strcmp(x,'earn')==1
       index = 3;
   elseif strcmp(x,'grain')==1
       index = 4;
   elseif strcmp(x,'interest')==1
       index = 5;   
   elseif strcmp(x,'money-fx')==1
       index = 6;
   elseif strcmp(x,'ship')==1
       index = 7;
   elseif strcmp(x,'trade')==1
       index = 8; 
   end
end
       
       
       
       
       
       
       
