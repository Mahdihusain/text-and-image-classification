clear;
clc;

train_data = importdata('../../traindata.txt');
test_data = importdata('../../testdata.txt');
train_label = importdata('../../trainlabels.txt');
test_label = importdata('../../testlabels.txt');

train_label(train_label==2)=-1;
test_label(test_label==2)=-1;

model = svmtrain(train_label, train_data, '-s 0 -t 2 -c 500 -g 2.5');
SV = full(model.SVs);
dlmwrite('../../svlibsvmgaussian.txt',SV);
[predict_label, accuracy, dec_values] = svmpredict(test_label, test_data, model); 