clear;
clc;

train_data = importdata('traindata.txt');
test_data = importdata('testdata.txt');
label = importdata('trainlabels.txt');
test_label = importdata('testlabels.txt');

label(label==2)=-1;
test_label(test_label==2)=-1;

rows = length(train_data(:,1));
b = ones(1,rows);
C = 500;

Q = zeros(rows);

for i=1:rows
    for j=1:rows
        Q(i,j) = label(i,1)*label(j,1)*(train_data(i,:)*train_data(j,:)');
    end
end

Q = Q*(-0.5);

cvx_begin
    variable alp(rows)
    maximize (alp'*Q*alp + b*alp)
    subject to
        alp >= 0
        alp <= C
        alp'*label == 0
cvx_end

support_vector = (alp>0.1);

w = zeros(1,length(train_data(1,:)));

for i=1:rows
    w = w + (alp(i,1)*label(i,1))*train_data(i,:);
end

b = 0;
for i=1:rows
    if (alp(i,:)>0.1 && alp(i,:)<C-0.1)
        b = label(i,1) - w*train_data(i,:)';
        break;
    end
end
    
%prediction

success = 0;
for i=1:length(test_data(:,1))
    temp = (w*test_data(i,:)' + b);
    if temp>0 && test_label(i,1)==1
        success = success +1;
    elseif temp<=0 && test_label(i,1)==-1
        success = success +1;
    end
end

acurracy = (success/length(test_data(:,1)))*100; 
        
    
    
    
 