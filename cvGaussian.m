clear;
clc;

train_data = importdata('../../traindata.txt');
test_data = importdata('../../testdata.txt');
train_label = importdata('../../trainlabels.txt');
test_label = importdata('../../testlabels.txt');

train_label(train_label==2)=-1;
test_label(test_label==2)=-1;

cv_accuracy = zeros(1,7);
test_accuracy = zeros(1,7);
cval = [0,1,2,3,4,5,6];

cv_accuracy(1,1) = svmtrain(train_label, train_data, '-s 0 -t 2 -c 1 -g 2.5 -v 10');
cv_accuracy(1,2) = svmtrain(train_label, train_data, '-s 0 -t 2 -c 10 -g 2.5 -v 10');
cv_accuracy(1,3) = svmtrain(train_label, train_data, '-s 0 -t 2 -c 100 -g 2.5 -v 10');
cv_accuracy(1,4) = svmtrain(train_label, train_data, '-s 0 -t 2 -c 1000 -g 2.5 -v 10');
cv_accuracy(1,5) = svmtrain(train_label, train_data, '-s 0 -t 2 -c 10000 -g 2.5 -v 10');
cv_accuracy(1,6) = svmtrain(train_label, train_data, '-s 0 -t 2 -c 100000 -g 2.5 -v 10');
cv_accuracy(1,7) = svmtrain(train_label, train_data, '-s 0 -t 2 -c 1000000 -g 2.5 -v 10');

model1 = svmtrain(train_label, train_data, '-s 0 -t 2 -c 1 -g 2.5');
model2 = svmtrain(train_label, train_data, '-s 0 -t 2 -c 10 -g 2.5');
model3 = svmtrain(train_label, train_data, '-s 0 -t 2 -c 100 -g 2.5');
model4 = svmtrain(train_label, train_data, '-s 0 -t 2 -c 1000 -g 2.5');
model5 = svmtrain(train_label, train_data, '-s 0 -t 2 -c 10000 -g 2.5');
model6 = svmtrain(train_label, train_data, '-s 0 -t 2 -c 100000 -g 2.5');
model7 = svmtrain(train_label, train_data, '-s 0 -t 2 -c 1000000 -g 2.5');

[predict_label, accuracy, dec_values] = svmpredict(test_label, test_data, model1); 
test_accuracy(1,1)=accuracy(1,1);
[predict_label, accuracy, dec_values] = svmpredict(test_label, test_data, model2);
test_accuracy(1,2)=accuracy(1,1);
[predict_label, accuracy, dec_values] = svmpredict(test_label, test_data, model3);
test_accuracy(1,3)=accuracy(1,1);
[predict_label, accuracy, dec_values] = svmpredict(test_label, test_data, model4);
test_accuracy(1,4)=accuracy(1,1);
[predict_label, accuracy, dec_values] = svmpredict(test_label, test_data, model5);
test_accuracy(1,5)=accuracy(1,1);
[predict_label, accuracy, dec_values] = svmpredict(test_label, test_data, model6);
test_accuracy(1,6)=accuracy(1,1);
[predict_label, accuracy, dec_values] = svmpredict(test_label, test_data, model7);
test_accuracy(1,7)=accuracy(1,1);

%plotting
figure;
plot(cval,test_accuracy,cval,cv_accuracy);
xlabel('log(C)');
ylabel('accuracy');
legend('test accuracy','cross validation accuracy');
